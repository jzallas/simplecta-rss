package com.capstone.rss;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends Activity implements
		ListView.OnItemClickListener {

	// CategoryFrag catfragment = new CategoryFrag();
	FeedsFrag feedsfragment = new FeedsFrag();
	ManageFrag managefragment = new ManageFrag();
	PostsFrag postfragment = new PostsFrag();
	SearchFrag searchfragment = new SearchFrag();
	SubscribeFrag subscribefragment = new SubscribeFrag();
	webViewFrag webViewFragment = new webViewFrag();
	InternalSearchFrag internalsearchfragment = new InternalSearchFrag();
	SharedPreferences mPrefs;

	FragmentManager fm = getFragmentManager();

	private String[] drawerListViewItems;
	private DrawerLayout drawerLayout;
	private ListView drawerListView;
	private CookieManager loginCookies = CookieManager.getInstance();

	protected static String username = "";

	protected static SimplectaWebView browser;
	protected static final String simplectaURL = "http://simplecta.appspot.com/";
	protected static boolean showMain = true;

	public String url_manage;
	public String html_manage;

	protected static String URLtoOpen;
	public String FeedURLtoOpen = "";
	public String TitleInMainBar = "RSS";
	public String INTERNAL_SEARCH_STRING = "";

	// to switch between posts when single post is opened
	public ArrayList<String> SHARED_POSTS_URLs = new ArrayList<String>();
	public int currentPost = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mPrefs = getSharedPreferences("username", 0);
		username = mPrefs.getString("username", "");

		// get list items from strings.xml
		drawerListViewItems = getResources().getStringArray(R.array.items);
		// get ListView defined in activity_main.xml
		drawerListView = (ListView) findViewById(R.id.left_drawer);

		// Set the adapter for the list view
		drawerListView.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_listview_item, drawerListViewItems));

		// listener for drawer item click
		drawerListView.setOnItemClickListener(this);

		// for setting drawer close
		drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		// added shadow on the drawer
		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);

		// themes the action bar with color and transparency
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.argb(140, 110,
				25, 180)));
		setupWebView();

		// starting fragment

		if (loggedIn()) {
			FragmentTransaction ft = fm.beginTransaction();

			ft.replace(R.id.MAINFRAG, postfragment);
			ft.commit();
		} else {
			FragmentTransaction ft = fm.beginTransaction();
			URLtoOpen = simplectaURL;
			ft.replace(R.id.MAINFRAG, webViewFragment);
			ft.commit();
			System.out.println("dont even get this far");
		}
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void setupWebView() {
		class MyJavaScriptInterface {
			@android.webkit.JavascriptInterface
			public void showHTML(String html) {
				// PARSE THE HTML RECEIVED HERE
				System.out.println("showHTML in main");
				// html contains raw url + html code, need to separate them
				String arr[] = html.split(" ", 2);
				String url = arr[0];
				String extracted_html = arr[1];

				parsePage(url, extracted_html);

			}
		}
		browser = (SimplectaWebView) findViewById(R.id.webView1);
		browser.setWebViewClient(new webClient());
		browser.getSettings().setJavaScriptEnabled(true);
		browser.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");

		try {
			Uri data = getIntent().getData();
			String url = data.toString();

			// url contains the potential rss.xml if this app was opened with an
			// intent
			if (!browser.simplecta_addRSS(url))
				Toast.makeText(this,
						"Cannot add RSS subscription\nNo network connection",
						Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			browser.loadUrl(simplectaURL);
		}

	}

	private void parsePage(String url, String html) {
		url_manage = url;
		html_manage = html;
		// get substring after "...simplecta.com/"
		String page = url.substring(simplectaURL.length());
		// split and get everything until the next "/"
		String arr[] = page.split("/", 2);
		page = arr[0];
		// determine which page we're on
		System.out.println("URL ---> " + url);
		if (url.equals(simplectaURL) || page.equals("all")) // WILL BE CALLED
															// WHEN ON
		// SIMPLECTA MAIN PAGE
		{
			Document doc = Jsoup.parse(html);
			SharedPreferences.Editor mEditor = mPrefs.edit();
			username = doc.getElementsByTag("body").get(0).ownText();
			username = username.substring(0, username.indexOf(' '));
			mEditor.putString("username", username).commit();
			Elements items = doc.getElementsByClass("item");
			DatabaseHelper db = new DatabaseHelper(getApplicationContext(),
					username);

			// go through each item element and populate feed and post array
			for (int i = 0; i < items.size(); i++) {
				// LOAD FEEDS
				Feed parsedFeed = new Feed();
				parsedFeed.setUrl(items.get(i).getElementsByClass("feedlink")
						.get(0).attr("href"));
				parsedFeed.setTitle(items.get(i).getElementsByClass("feedlink")
						.get(0).ownText());
				db.insertFeed(parsedFeed);

				// LOAD POSTS

				Post parsedPost = new Post();
				parsedPost.setUrl(items.get(i).getElementsByClass("item_links")
						.get(0).getElementsByClass("peek").get(0).attr("href"));
				parsedPost.setTitle(items.get(i)
						.getElementsByClass("item_links").get(0)
						.getElementsByClass("read_link").get(0).ownText());
				parsedPost.setKey(items.get(i).getElementsByClass("item_links")
						.get(0).getElementsByClass("ajax_link").get(0)
						.attr("data-key"));
				db.insertPost(parsedPost, parsedFeed);

			}

			// database now has all posts with keys linked to feeds (without
			// key) if they werent already there
			// 'username' contains the string username found on main page

			db.closeDB();

		} else if (page.equals("feeds")) // WILL BE CALLED WHEN ON SIMPLECTA
											// MANAGE PAGE
		{
			DatabaseHelper db = new DatabaseHelper(getApplicationContext(),
					username);
			Document doc = Jsoup.parse(html);
			Elements feedlinks = doc.getElementsByClass("largefeedlink");
			Elements feedkeys = doc.getElementsByClass("peek");

			for (int i = 0; i < feedlinks.size(); i++) {
				Feed parsedFeed = new Feed();
				parsedFeed.setUrl(feedlinks.get(i).attr("href"));
				parsedFeed.setTitle(feedlinks.get(i).ownText());
				parsedFeed.setKey(feedkeys.get(i).attr("href"));
				db.insertFeed(parsedFeed);
			}
			// database now has new feeds with keys if they werent already there
			db.closeDB();

		} else if (page.equals("feed")) // WILL BE CALLED WHEN ON SIMPLECTA
										// SPECIFIC
										// FEED PAGE
		{

			// figure out which feed it's referencing
			Feed feed = new Feed();
			feed.setUrl(arr[1]); // now feed has the url

			DatabaseHelper db = new DatabaseHelper(getApplicationContext(),
					username);
			Document doc = Jsoup.parse(html);
			String website = doc.getElementsByClass("largefeedlink").get(0)
					.ownText();
			String website_url = doc.getElementsByClass("largefeedlink").get(0)
					.attr("href");
			Elements rawPosts = doc.getElementsByClass("read_link");

			for (Element element : rawPosts) {
				Post parsedPost = new Post();
				parsedPost.setUrl(element.attr("href"));
				parsedPost.setTitle(element.ownText());
				db.insertPost(parsedPost, feed);
			}
			// database now has all posts linked to this feed (neither has key)
			// if they werent already there
			// variable website has website name, not the rss name: ie
			// 'ENGADGET'
			// variable website_url has the url to homepage : ie
			// 'http://www.engadget.com/'
			db.closeDB();

		} else if (page.equals("markRead")) {
			Document doc = Jsoup.parse(html);
			String confirmation = doc.getElementsByTag("body").get(0).ownText();
			// THIS STRING WILL HAVE "OK" IF IT SUCCESSFULLY MARKS SOMETHING
			// UNREAD
			// OTHERWISE, IT WAS UNSUCCESSFUL
		} else if (page.equals("markUnread")) {
			Document doc = Jsoup.parse(html);
			String confirmation = doc.getElementsByTag("body").get(0).ownText();
			// THIS STRING WILL HAVE "OK" IF IT SUCCESSFULLY MARKS SOMETHING
			// UNREAD
			// OTHERWISE, IT WAS UNSUCCESSFUL
		} else if (page.equals("addRSS")) {
			System.out.println("trying to subscribe to feed ");

			if (html.contains("internal server error"))
				subscribefragment.toastMessageForSubscribe(false);
			else {
				System.out.println("subscribed");
				subscribefragment.toastMessageForSubscribe(true);
			}
		}
	}

	protected boolean loggedIn() {
		if (loginCookies.getCookie(simplectaURL) != null) {
			return true;
		}
		return false;
	}

	public void sharePost(String uri) {
		System.out.println("Sharing linK: " + uri);
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_TEXT, uri);
		startActivity(Intent.createChooser(shareIntent, "Share..."));
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void onItemClick(AdapterView parent, View view, int position, long id) {

		if (loggedIn()) {
			FragmentTransaction ft = fm.beginTransaction();

			switch (position) {
			case 0:
				ft.replace(R.id.MAINFRAG, feedsfragment);

				break;
			case 1:
				FeedURLtoOpen = "";
				TitleInMainBar = "RSS";
				ft.replace(R.id.MAINFRAG, postfragment);

				break;
			case 2:
				TitleInMainBar = "Subscribe to Feed";
				ft.replace(R.id.MAINFRAG, subscribefragment);
				break;
			case 3:
				ft.replace(R.id.MAINFRAG, internalsearchfragment);
				break;
			}

			ft.addToBackStack(null);
			ft.commit();
		}

		drawerLayout.closeDrawer(drawerListView);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		// getMenuInflater().inflate(R.menu.main, menu);
		if (Build.VERSION.SDK_INT >= 11) {
			selectMenu(menu);
		}

		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		if (Build.VERSION.SDK_INT < 11) {
			selectMenu(menu);
		}
		return super.onPrepareOptionsMenu(menu);
	}

	private void selectMenu(Menu menu) {
		menu.clear();
		if (loggedIn())
			getMenuInflater().inflate(R.menu.main, menu);
		else
			getMenuInflater().inflate(R.menu.main_nologout, menu);
	}

	protected void invalidate() {
		if (Build.VERSION.SDK_INT >= 11) {
			invalidateOptionsMenu();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// case R.id.action_settings:
		// Intent intent_settings = new Intent(this, SettingsActivity.class);
		// startActivity(intent_settings);
		// break;
		case R.id.log_out:
			browser.loadUrl("about:blank");
			loginCookies.removeAllCookie();
			invalidate();
			username = "";
			SharedPreferences.Editor mEditor = mPrefs.edit();
			mEditor.putString("username", "").commit();
			FragmentTransaction ft = fm.beginTransaction();
			URLtoOpen = simplectaURL;
			ft.replace(R.id.MAINFRAG, webViewFragment);
			ft.commit();
			break;
		case R.id.action_search:
			if (loggedIn()) {
				FragmentTransaction ft1 = fm.beginTransaction();
				ft1.replace(R.id.MAINFRAG, internalsearchfragment);
				ft1.addToBackStack(null);
				ft1.commit();
			}
			return true;
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
