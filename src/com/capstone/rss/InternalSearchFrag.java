package com.capstone.rss;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

public class InternalSearchFrag extends Fragment implements OnClickListener{
	private EditText Internal_Search_TextField;
	Button Internal_Search_Button;
	private EditText External_Search_TextField;
	Button External_Search_Button;
	
	@Override
   public View onCreateView(LayoutInflater inflater,
      ViewGroup container, Bundle savedInstanceState) {
		RelativeLayout myView = (RelativeLayout) inflater.inflate(R.layout.activity_internal_search, container, false);
		
		Internal_Search_Button = (Button)myView.findViewById(R.id.button_Internal_Search);
		Internal_Search_Button.setOnClickListener( this);
		Internal_Search_TextField = (EditText)myView.findViewById(R.id.Internal_Search_TextField);		
		
		External_Search_Button = (Button)myView.findViewById(R.id.button_External_Search);
		External_Search_Button.setOnClickListener( this);
		External_Search_TextField = (EditText)myView.findViewById(R.id.External_Search_TextField);
		
		  

		
		return myView; // super.onCreateView(inflater, container, savedInstanceState);
   }  
	
	private void internalSearch(String queryString)
	{
		System.out.println("Searching : " + queryString);
		((MainActivity) getActivity()).FeedURLtoOpen = "INTERNAL_SEARCH";
		((MainActivity) getActivity()).INTERNAL_SEARCH_STRING = queryString;
		Internal_Search_TextField.setText("");
	
// HERE SHOULD BE LOGIC TO AUTOMAGICALLY SWITCH TO POSTS FRAGMENT 
		FragmentTransaction ft = ((MainActivity) getActivity()).fm
				.beginTransaction();
		ft.replace(R.id.MAINFRAG,
				((MainActivity) getActivity()).postfragment);
		ft.addToBackStack(null);
		ft.commit();
	}
	
	private void externalSearch(String queryString)
	{
		System.out.println("External ! Searching : " + queryString);
		String EXTERNAL_SEARCH_PAGE = "http://www.rsssearchhub.com/feeds/?search=";
		((MainActivity) getActivity()).URLtoOpen= EXTERNAL_SEARCH_PAGE + queryString; 
		
		FragmentTransaction ft = ((MainActivity) getActivity()).fm.beginTransaction();
		ft.replace(R.id.MAINFRAG, ((MainActivity) getActivity()).webViewFragment);
		ft.addToBackStack(null);
		ft.commit();
		External_Search_TextField.setText("");
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {		
			case R.id.button_Internal_Search:
				String q = Internal_Search_TextField.getText().toString();
				internalSearch(q);
				break;
			case R.id.button_External_Search:
				String qe = External_Search_TextField.getText().toString();
				externalSearch(qe);
				break;
				
				// ((MainActivity) getActivity()).URLtoOpen = 
		}
		final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
	}
}
