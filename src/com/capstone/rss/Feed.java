package com.capstone.rss;

public class Feed {
	private int id;
	private String rsslink;
	private String title;
	private String key;

	protected void setUrl(String url) {
		url = url.replace("/feed/", "");
		url = url.replace("%3a", ":");
		url = url.replace("%2f", "/");
		if (url.startsWith("?")){
			url = url.substring(1);
		}
		this.rsslink = url;		
	}
	
	protected void setID(int id){
		this.id = id;
	}
	
	protected int getID(){
		return id;
	}

	protected String getUrl() {
		return rsslink;
	}
	
	protected String getTitle()
	{
		return title;
	}
	
	protected void setTitle(String title){
		this.title = title;
	}
	
	protected void setKey(String key){
		if (key != null)
		{
			key = key.replace("/unsubscribe/", "");
			if (key.startsWith("?")){
				key = key.substring(1);
			}
		}
		this.key = key;
	}
	
	protected String getKey(){
		return key;
	}
}
