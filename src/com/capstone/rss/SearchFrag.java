package com.capstone.rss;


import android.app.Fragment;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

//http://www.androidpeople.com/android-listview-searchbox-sort-items
public class SearchFrag extends Fragment implements OnClickListener {
	public static final int TEXTFIELD_WIDTH = 200;
	WebView searchBrowser;
	Button Search, backButton;
	TextView textview1,textview2, textview3, textview4, textview5;  
	
	private EditText ed;
	private ArrayList<String> urls;
	private ArrayList<String> texts;
	private ArrayList<String> urlsEn;
	private String postUrl;
	int textlength = 0;
	int currentPage = 0; // for pagination 
	private static final int SWIPE_MIN_DISTANCE = 20; // 8;
	private static final int SWIPE_MAX_OFF_PATH = 150; // 280 // 200;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	
	private GestureDetector gestureDetector; // imported GestureDetector
	View.OnTouchListener gestureListener;
	
	@SuppressWarnings("deprecation")
	@Override
	   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	      /**
	       * Inflate the layout for this fragment
	       */
		RelativeLayout myView = (RelativeLayout) inflater.inflate(R.layout.activity_search, container, false);
		ed = (EditText)myView.findViewById(R.id.EditText01);
	 	searchBrowser = (WebView) myView.findViewById(R.id.webViewSearch);
	 	searchBrowser.setWebViewClient(new webClient());
//	 	searchBrowser.getSettings().setJavaScriptEnabled(true); //   
	 	searchBrowser.setVisibility(8);
	 	
	 	Search = (Button)myView.findViewById(R.id.button1);
		Search.setOnClickListener(this);
		
		backButton = (Button)myView.findViewById(R.id.buttonBack);
		backButton.setOnClickListener(this);
		
		textview1 = (TextView)myView.findViewById(R.id.urlTextView1);
		textview1.setOnClickListener(this);
		textview2 = (TextView)myView.findViewById(R.id.urlTextView2);
		textview2.setOnClickListener(this);
		textview3 = (TextView)myView.findViewById(R.id.urlTextView3);
		textview3.setOnClickListener(this);
		textview4 = (TextView)myView.findViewById(R.id.urlTextView4);
		textview4.setOnClickListener(this);
		textview5 = (TextView)myView.findViewById(R.id.urlTextView5);
		textview5.setOnClickListener(this);
		
		gestureDetector = new GestureDetector(new MyGestureDetector());
		gestureListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		};
		
		textview1.setOnTouchListener(gestureListener);
		textview2.setOnTouchListener(gestureListener);
		textview3.setOnTouchListener(gestureListener);
		textview4.setOnTouchListener(gestureListener);
		textview5.setOnTouchListener(gestureListener);
		
		postUrl = "";
		setupWebView("");
		return myView;
	   }

	private void setupWebView(String query){
 	class MyJavaScriptInterface   
 	{  
 	    @android.webkit.JavascriptInterface 
 	    public void showHTML(String html)  
 	    {  
 	    	if(html.contains("www.plazoo.com") && html.contains("clr2d3539")){
 	    		System.out.println("Loading plazoo search!");
 	    		Document doc = Jsoup.parse(html); 
     	        Elements text = doc.getElementsByClass("clr2d3539");
     	    	Elements link = doc.getElementsByClass("clr999");
     	    	
     	    	urlsEn = new ArrayList<String>();
     	    	for(int i = 0; i < 5; i++){
     	    		urlsEn.add(doc.getElementsByClass("clrCCC").get(i).attr("href"));
     	    	}
 	    		urls = new ArrayList<String>();
     	    	texts = new ArrayList<String>();  
     	    	
     	    	for (Element element : text) {
     	    		texts.add(element.ownText());
     	    	}
     	    	texts.remove(0); // dont need the first one -> it's not content
     	    	int i = 0;
     	    	for (Element element : link) {
     	    		if(i % 3 == 0){
     	    			urls.add(element.ownText());
//         	    		System.out.println(i+" --> "+element.ownText());	
     	    		}
     	    		i++;
     	    	}
     	    	urls = formatUrls(urls);
     			System.out.println("Setting fields in showHTML()");
     			getActivity().runOnUiThread(new Runnable() {
     		        @Override
     		        public void run() {
     		        	if(texts.get(0).length() > TEXTFIELD_WIDTH)
     		        		textview1.setText(texts.get(0).subSequence(0, TEXTFIELD_WIDTH));
     		        	else
     		        		textview1.setText(texts.get(0));
     		        	
     		        	if(texts.get(1).length() > TEXTFIELD_WIDTH)
     		        		textview2.setText(texts.get(1).subSequence(0, TEXTFIELD_WIDTH));
     		        	else
     		        		textview2.setText(texts.get(1));
     		        	
     		        	if(texts.get(2).length() > TEXTFIELD_WIDTH)
     		        		textview3.setText(texts.get(2).subSequence(0, TEXTFIELD_WIDTH));
     		        	else
     		        		textview3.setText(texts.get(2));
     		        	
     		        	if(texts.get(3).length() > TEXTFIELD_WIDTH)
     		        		textview4.setText(texts.get(3).subSequence(0, TEXTFIELD_WIDTH));
     		        	else
     		        		textview4.setText(texts.get(3));
     		        	
     		        	if(texts.get(4).length() > TEXTFIELD_WIDTH)
     		        		textview5.setText(texts.get(4).subSequence(0, TEXTFIELD_WIDTH));
     		        	else
     		        		textview5.setText(texts.get(4));
     		        }
     			});
 	    	}
 	    	else if( html.contains("InfoBox2") ){
 	    		Document doc = Jsoup.parse(html);
 	    		Elements a = doc.select("a[href^=/go.asp]");
 	    		String b = a.toString();
 	    		int indexOfQuotation = b.indexOf("\"", 10);
 	    		postUrl = b.substring(9,indexOfQuotation);
 	    		System.out.println("Loading single page 1 : " + postUrl);
 	    		loadSinglePostPage(postUrl);
 	    	}
 	    	else { // load the post here
 	    		System.out.println("finally loading the link: " + postUrl);
// 	    		System.out.println(html);
 	    		getActivity().runOnUiThread(new Runnable() {
     		        @Override
     		        public void run() {
     		        	searchBrowser.setVisibility(0);
//     		        	textview1.setVisibility(getView().INVISIBLE); //
//     		        	textview2.setVisibility(getView().GONE);
//     		        	textview3.setVisibility(View.INVISIBLE);
//     		        	textview4.setVisibility(8);
//     		        	textview5.setVisibility(8);
     		        	searchBrowser.loadUrl(postUrl); // only "postUrl"
     		        }
 	    		});
 	    	}
 	    	
 	    }
 	}
 	 	
 	searchBrowser.setWebViewClient(new webClient());
 	searchBrowser.getSettings().setJavaScriptEnabled(true);  
 	searchBrowser.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT"); 
 	searchBrowser.setVisibility(8);
 	//        browser.loadUrl("http://www.plazoo.com/en/default.asp?q=" + query);
 }
	
	private ArrayList<String> formatUrls(ArrayList<String> array) {
		for (int i = 0; i < array.size(); i++) {
			int startIndex = array.get(i).indexOf(">");
			int endIndex = array.get(i).indexOf("-");
			String replacement = "";
			String toBeReplaced = array.get(i).substring(startIndex + 1, endIndex+2);
			array.set(i, array.get(i).replace(toBeReplaced, replacement));
		}
		return array;
	}

	private void loadSinglePostPage(String link) {
		final String linkFinal = "http://www.plazoo.com"+ link;
		System.out.println("loading single page 2--> "+ linkFinal);
		getActivity().runOnUiThread(new Runnable() {
		        @Override
		        public void run() {
		        	searchBrowser.loadUrl(linkFinal);
		        }
        });
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {		
		case R.id.button1:

			urls  = null;
	    	texts = null;
	    	urlsEn = null;
	    	if(textview1 != null && textview2 != null && textview3 != null &&
	    			textview4 != null && textview1 != null){
	    		textview1.setText("");
	        	textview2.setText("");
	        	textview3.setText("");
	        	textview4.setText("");
	        	textview5.setText("");	
	    	}
	    	currentPage = 0;
			String q = ed.getText().toString();
			System.out.println("loading page:" + "http://www.plazoo.com/en/default.asp?q=" + q);
			searchBrowser.loadUrl("http://www.plazoo.com/en/default.asp?q=" + q);
			break;
		case R.id.buttonBack:
			
			textview1.setText("");
        	textview2.setText("");
        	textview3.setText("");
        	textview4.setText("");
        	textview5.setText("");
        	searchBrowser.setVisibility(View.GONE);
        	urls  = null;
	    	texts = null;
	    	urlsEn = null;
			break;
		case R.id.urlTextView1:	
			if(texts != null || urlsEn != null || urlsEn != null)
				searchBrowser.loadUrl("http://www.plazoo.com"+urlsEn.get(0));
			break;
		case R.id.urlTextView2:
			if(texts != null || urlsEn != null || urlsEn != null)
				searchBrowser.loadUrl("http://www.plazoo.com"+urlsEn.get(1));
			break;
		case R.id.urlTextView3:
			if(texts != null || urlsEn != null || urlsEn != null)
				searchBrowser.loadUrl("www.plazoo.com"+urlsEn.get(2));
			break;
		case R.id.urlTextView4:
			if(texts != null || urlsEn != null || urlsEn != null)
				searchBrowser.loadUrl("http://www.plazoo.com"+urlsEn.get(3));
			break;
		case R.id.urlTextView5:
			if(texts != null || urlsEn != null || urlsEn != null)
				searchBrowser.loadUrl("http://www.plazoo.com"+urlsEn.get(4));
			break;
		}
	}
	
	class MyGestureDetector extends SimpleOnGestureListener { // imported
		// SimpleOnGestureListener
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {
				if (Math.abs(velocityY) > Math.abs(velocityX)) {
					if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE // ){
							&& Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
						onSwipeTop();
					} else {
						onSwipeBottom();
					}
				}
				else if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE // ){
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					onSwipeLeft();

				} else { 
					onSwipeRight();
				}
			} catch (Exception e) {
				// nothinG
			}
			return false;
		}

		private void onSwipeLeft() {
			// TODO Auto-generated method stub
			System.out.println("left");	
		}

		private void onSwipeRight() {
			// TODO Auto-generated method stub
			System.out.println("right");
		}

		private void onSwipeBottom() {
			// TODO Auto-generated method stub
			System.out.println("bottom");
			if(currentPage >= 1)
				currentPage -= 1;
			setNextTextField(currentPage);
		}

		private void onSwipeTop() {
			// TODO Auto-generated method stub
			System.out.println("top");
//			if(currentPage*5+4 < texts.size())
				currentPage += 1;
			setNextTextField(currentPage);
		}
	}
	
	void setNextTextField(final int page) {
		getActivity().runOnUiThread(new Runnable() {
	        @Override
	        public void run() {
	        	if(page >= 0 && page*5+4 < texts.size()){
	        		System.out.println("paginating! page is "+ page);
	        		if(texts.get(currentPage*5).length() > TEXTFIELD_WIDTH)
		        		textview1.setText(texts.get(currentPage*5).subSequence(0, TEXTFIELD_WIDTH));
		        	else
		        		textview1.setText(texts.get(currentPage*5));
		        	
		        	if(texts.get(currentPage*5+1).length() > TEXTFIELD_WIDTH)
		        		textview2.setText(texts.get(currentPage*5+1).subSequence(0, TEXTFIELD_WIDTH));
		        	else
		        		textview2.setText(texts.get(currentPage*5+1));
		        	
		        	if(texts.get(currentPage*5+2).length() > TEXTFIELD_WIDTH)
		        		textview3.setText(texts.get(currentPage*5+2).subSequence(0, TEXTFIELD_WIDTH));
		        	else
		        		textview3.setText(texts.get(currentPage*5+2));
		        	
		        	if(texts.get(currentPage*5+3).length() > TEXTFIELD_WIDTH)
		        		textview4.setText(texts.get(currentPage*5+3).subSequence(0, TEXTFIELD_WIDTH));
		        	else
		        		textview4.setText(texts.get(currentPage*5+3));
		        	
		        	if(texts.get(currentPage*5+4).length() > TEXTFIELD_WIDTH)
		        		textview5.setText(texts.get(currentPage*5+4).subSequence(0, TEXTFIELD_WIDTH));
		        	else
		        		textview5.setText(texts.get(currentPage*5+4));
	        	}
	        	else if (page*5+4 >= texts.size()){
	        		String q = ed.getText().toString();
//	        		http://www.plazoo.com/en/default.asp?q=news&p=5
	        		System.out.println("getting next --> " + "http://www.plazoo.com/en/default.asp?q=" + q+"&p="+currentPage);
	    			searchBrowser.loadUrl("http://www.plazoo.com/en/default.asp?q=" + q+"&p="+currentPage);
	        	}
	        }
		});
	}
}
