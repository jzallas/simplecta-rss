package com.capstone.rss;

import android.app.Activity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class webClient extends WebViewClient {

	@Override
	public boolean shouldOverrideUrlLoading(WebView view, String url) {
		view.loadUrl(url);
		return true;
	}

	
	
	@Override
	public void onPageFinished(WebView view, String url){
		if (url.contains("simplecta"))
			view.loadUrl("javascript:window.HTMLOUT.showHTML(document.URL + ' ' + document.getElementsByTagName('html')[0].innerHTML);");
		else if (url.contains("plazoo.com"))  // vitaliy's case
			view.loadUrl("javascript:window.HTMLOUT.showHTML('<head>'+document.getElementsByTagName('html')[0].innerHTML+'</head>');");
		view.clearHistory();
		view.clearCache(true);
	}
}
