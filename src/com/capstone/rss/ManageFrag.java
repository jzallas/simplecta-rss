package com.capstone.rss;

import java.util.ArrayList;
import android.content.DialogInterface;
import android.content.Intent;

import java.util.List;

import android.app.AlertDialog;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class ManageFrag extends ListFragment{
	protected ArrayList<String> Feeds = new ArrayList<String>();
	private ArrayList<String> URLs = new ArrayList<String>();
	private ArrayList<String> keys = new ArrayList<String>();
	
	private ArrayAdapter<String> adapter;
		

	@SuppressWarnings("deprecation")
	@Override
	public void onListItemClick(ListView l, View v, final int position, long id) {
		AlertDialog alertDialog = new AlertDialog.Builder(this.getActivity()).create();
		alertDialog.setTitle("Options");
		alertDialog.setMessage("Feed: " + Feeds.get(position));
		alertDialog.setButton("Cancel", new DialogInterface.OnClickListener() {
		   public void onClick(DialogInterface dialog, int which) {
		      // TODO Add your code for the button here.
			   System.out.println("Clicked Cancel ");
		   }
		});
		alertDialog.setButton2("Unsubscribe", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			   unsubscribeFromFeedByFeedURLAndKey(URLs.get(position), keys.get(position));
			   System.out.println("Clicked Unsubscribe ");
		   }
		});
		alertDialog.setButton3("Share", new DialogInterface.OnClickListener() {
			   public void onClick(DialogInterface dialog, int which) {
			      // TODO Add your code for the button here.
				   shareFeed(URLs.get(position), Feeds.get(position)); 
				   System.out.println("Clicked Share ");
			   }
			});
//		alertDialog.setButton("test", new DialogInterface.OnClickListener() {
//			   public void onClick(DialogInterface dialog, int which) {
//			      // TODO Add your code for the button here.
//				   System.out.println("Clicked test ");
//			   }
//			});
		alertDialog.show();
		MainActivity.browser.loadUrl(Feeds.get(position));
	}
	
	private void shareFeed(String feedURL, String title)
	{
		System.out.println("Sharing Feed: " + title);
		System.out.println("Link is: " + feedURL);
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		  shareIntent.setType("text/plain");
		  shareIntent.putExtra(Intent.EXTRA_TEXT, feedURL + "\n" + title);
		  startActivity(Intent.createChooser(shareIntent, "Share..."));
	}
	
	
	@Override
   public View onCreateView(LayoutInflater inflater,
      ViewGroup container, Bundle savedInstanceState) {
		
		adapter = new ArrayAdapter<String>(
				inflater.getContext(), android.R.layout.simple_list_item_1, Feeds);
		
		
		populateFeeds();
		setListAdapter(adapter);
		return super.onCreateView(inflater, container, savedInstanceState);
   }
	
		
	private void unsubscribeFromFeedByFeedURLAndKey(String feedURL, String feedKey )
	{
		DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext(), MainActivity.username);
		System.out.println("trying to unsubscribe from: " + feedURL);
		
		db.deleteFeed(feedURL, true); //
		
		unsubscribeFromFeedInSimplecta(feedKey, feedURL);
		
		adapter.clear();
		
        db.closeDB();
        populateFeeds();		
	}
	
	private void unsubscribeFromFeedInSimplecta(final String link, final String url)
	{
		getActivity().runOnUiThread(new Runnable() {
	        @Override
	        public void run() {
				if( MainActivity.browser.simplecta_unsubscribe(link)) {
					// unsubscribe from Simplecta Server's
					Toast toast = Toast.makeText(getActivity(), "Unsubscribed from " + url,
							Toast.LENGTH_SHORT);
					toast.show();	
				} 
				else {
					Toast toast = Toast.makeText(getActivity(), "Can't unsubscribe. Please check your network connection",
							Toast.LENGTH_SHORT);
					toast.show();	
				}
	        }});
	}
	
	private void populateFeeds()
	{
		DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext(), MainActivity.username);
		Feeds.clear();
		URLs.clear();
		keys.clear();
		List<Feed> allFeeds = db.getAllFeeds();
		System.out.println("size of allFeeds ---->> : "+ allFeeds.size());
		for (int i = 0; i < allFeeds.size(); i++) {
			Feeds.add(allFeeds.get(i).getTitle());
			URLs.add(allFeeds.get(i).getUrl());
			keys.add(allFeeds.get(i).getKey());
			System.out.println("feed: " + allFeeds.get(i).getTitle());
		}
		
		db.closeDB();
	}
}