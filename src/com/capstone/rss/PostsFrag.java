package com.capstone.rss;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class PostsFrag extends ListFragment implements OnItemLongClickListener {

	private ArrayList<String> titles = new ArrayList<String>();
	private ArrayList<String> URLs = new ArrayList<String>();
	private ArrayList<String> keys = new ArrayList<String>();
	private ArrayList<Boolean> readStatus = new ArrayList<Boolean>();
	MenuItem backButton;
	ArrayAdapter<String> adapter;
	String xRead = "[X]";

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {

		((MainActivity) getActivity()).URLtoOpen = URLs.get(position);
		((MainActivity) getActivity()).currentPost = position;
		DatabaseHelper db = new DatabaseHelper(getActivity()
				.getApplicationContext(), MainActivity.username);
		Post post = db.getPost(db.getPostID(URLs.get(position)));
		Feed feed = db.getFeedOfPost(URLs.get(position));
		if (!readStatus.get(position)) { // unread
			post.setRead(true);
			readStatus.set(position, true);
			if (keys.get(position) != null)
				MainActivity.browser.simplecta_markRead(keys.get(position));
		}
		db.insertPost(post, feed);

		db.closeDB();
		
		updateRead(position);


		FragmentTransaction ft = ((MainActivity) getActivity()).fm
				.beginTransaction();
		ft.replace(R.id.MAINFRAG,
				((MainActivity) getActivity()).webViewFragment);
		ft.addToBackStack(null);
		ft.commit();
	}
	
	Runnable run = new Runnable(){
	     public void run(){
	    	 System.out.println("NOTIFY GETS CALLED FIRST");
	    	 adapter.notifyDataSetChanged();
	     }
	};

	private boolean updateRead(int position) {
		String s = titles.get(position);
		titles.remove(position);
		if (readStatus.get(position)) {
			titles.set(position, "[" + '\t' + "] " + s.substring(4));
			readStatus.remove(position);
			readStatus.add(position, false);
		} else {
			titles.set(position, "[X] " + s.substring(4));
			readStatus.remove(position);
			readStatus.add(position, true);
		}
		adapter.notifyDataSetChanged();
		return true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		((MainActivity) getActivity()).SHARED_POSTS_URLs.clear();
		ActionBar ab = ((MainActivity) getActivity()).getActionBar();
		ab.setTitle(((MainActivity) getActivity()).TitleInMainBar);

		// fetchPostsByFeed();
		populateLists(true);

		// ((MainActivity) getActivity()).setTitle(((MainActivity)
		// getActivity()).TitleInMainBar);
		if (titles.size() == 0)
			titles.add("Refresh to get posts from Simplecta");
		adapter = new ArrayAdapter<String>(inflater.getContext(),
				android.R.layout.simple_list_item_1, titles);

		setListAdapter(adapter);
		setHasOptionsMenu(true);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	private boolean resumeHasRun = false;

	@Override
	public void onResume() {
		super.onResume();
		if (!resumeHasRun) {
			resumeHasRun = true;
			return;
		}
		ActionBar ab = ((MainActivity) getActivity()).getActionBar();
		ab.setTitle(((MainActivity) getActivity()).TitleInMainBar);
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean onItemLongClick(final AdapterView<?> arg0, View arg1, final int position,
			long id) {
		
		final String markReadUnread;
		boolean prev = readStatus.get(position);
		if(!readStatus.get(position)) { // unread
			markReadUnread = "Mark Read";
		}
		else { // read
			markReadUnread = "Mark Unread";
		}
		AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
		alertDialog.setTitle("Options");
		alertDialog.setMessage("Feed: " + titles.get(position));
		
		alertDialog.setButton("Cancel", new DialogInterface.OnClickListener() {
		   public void onClick(DialogInterface dialog, int which) {
			   System.out.println("Clicked Cancel ");
			   // dismiss
		   }
		});
		
		alertDialog.setButton2("Share", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				((MainActivity)getActivity()).sharePost(URLs.get(position));	  
		   }
		});
		
		if(readStatus.get(position)){
		alertDialog.setButton3(markReadUnread, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				DatabaseHelper db = new DatabaseHelper(getActivity()
						.getApplicationContext(), MainActivity.username);
				Post post = db.getPost(db.getPostID(URLs.get(position)));
				Feed feed = db.getFeedOfPost(URLs.get(position));
				if(!readStatus.get(position)){ // unread
					post.setRead(true);
					readStatus.set(position, true);
					if (keys.get(position)!=null) MainActivity.browser.simplecta_markRead(keys.get(position));
				}
				else { // read
					post.setRead(false);
					readStatus.set(position, false);
					if (keys.get(position)!=null) MainActivity.browser.simplecta_markUnread(keys.get(position));
				}
				db.insertPost(post, feed);
				db.closeDB();
				updateRead(position);
				
				}
		});
		}
		alertDialog.show();
		
				
		return true;
	}
	

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		final InputMethodManager imm = (InputMethodManager) getActivity()
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
//		OnItemLongClickListener listener = new OnItemLongClickListener() {
//			@SuppressWarnings("deprecation")
//			@Override
//			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
//					final int position, long id) {
//				AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
//						.create();
//				alertDialog.setTitle("Options");
//				alertDialog.setMessage("Feed: " + titles.get(position));
//
//				alertDialog.setButton("Cancel",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								System.out.println("Clicked Cancel ");
//								// dismiss
//							}
//						});
//
//				alertDialog.setButton2("Share",
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								((MainActivity) getActivity()).sharePost(URLs
//										.get(position));
//							}
//						});
//				final String markReadUnread;
//				if (!readStatus.get(position)) { // unread
//					markReadUnread = "Mark Read";
//				} else { // read
//					markReadUnread = "Mark Unread";
//				}
//
//				alertDialog.setButton3(markReadUnread,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								DatabaseHelper db = new DatabaseHelper(
//										getActivity().getApplicationContext(),
//										MainActivity.username);
//								Post post = db.getPost(db.getPostID(URLs
//										.get(position)));
//								Feed feed = db.getFeedOfPost(URLs.get(position));
//								if (!readStatus.get(position)) { // unread
//									post.setRead(true);
//									readStatus.set(position, true);
//									if (keys.get(position) != null)
//										MainActivity.browser
//												.simplecta_markRead(keys
//														.get(position));
//								} else { // read
//									post.setRead(false);
//									readStatus.set(position, false);
//									if (keys.get(position) != null)
//										MainActivity.browser
//												.simplecta_markUnread(keys
//														.get(position));
//								}
//								db.insertPost(post, feed);
//								db.closeDB();
//							}
//						});
//
//				alertDialog.show();
//
//				return true;
//			}
//		};
		getListView().setOnItemLongClickListener(this);
	}

	private void populateLists(boolean firstCall) {

		titles.clear();
		keys.clear();
		readStatus.clear();
		URLs.clear();

		DatabaseHelper db = new DatabaseHelper(getActivity()

		.getApplicationContext(), MainActivity.username);

		List<Post> allPosts;

		if (firstCall)
			loadPage();

		if (((MainActivity) getActivity()).FeedURLtoOpen.isEmpty()) {
			System.out.println("feed url is empty");
			allPosts = db.getAllPosts();

		}

		// this case is fetching posts from Internal Search jazz
		else if (((MainActivity) getActivity()).FeedURLtoOpen == "INTERNAL_SEARCH") {
			System.out.println("internal search");
			allPosts = db
					.searchPosts(((MainActivity) getActivity()).INTERNAL_SEARCH_STRING); // <----
																							// this
																							// will
																							// return
																							// a
																							// List
																							// of
																							// Posts
																							// that
																							// contain
																							// the
																							// phrase
																							// "iPad"
																							// in
																							// the
																							// title;
			((MainActivity) getActivity())
					.setTitle(((MainActivity) getActivity()).INTERNAL_SEARCH_STRING);
		} else

			allPosts = db
					.getAllPostsByFeed(((MainActivity) getActivity()).FeedURLtoOpen);

		db.closeDB();

		((MainActivity) getActivity()).SHARED_POSTS_URLs.clear();

		for (int i = 0; i < allPosts.size(); i++) {
			if (allPosts.get(i).getRead()){
				char c = 10003;
				titles.add("["+c+"] " + allPosts.get(i).getTitle());
			}
			else
			{
				char c = 8195;
				titles.add("[" + c + "] " + allPosts.get(i).getTitle());
			}

			keys.add(allPosts.get(i).getKey());
			readStatus.add(allPosts.get(i).getRead());
			URLs.add(allPosts.get(i).getUrl());

			((MainActivity) getActivity()).SHARED_POSTS_URLs.add(allPosts
					.get(i).getUrl());

		}

	}

	// private void fetchPostsByFeed()
	// {
	// titles.clear();
	// URLs.clear();
	// keys.clear();
	// readStatus.clear();
	// ((MainActivity) getActivity()).SHARED_POSTS_URLs.clear();
	// MainActivity.browser.simplecta_feed(((MainActivity)
	// getActivity()).FeedURLtoOpen);
	//
	// DatabaseHelper db = new DatabaseHelper(getActivity()
	// .getApplicationContext(), MainActivity.username);
	// List<Post> allPosts;
	// if (((MainActivity) getActivity()).FeedURLtoOpen != "XXYXX" ) {
	// allPosts = db.getAllPostsByFeed(((MainActivity)
	// getActivity()).FeedURLtoOpen);
	// // ((MainActivity) getActivity()).FeedURLtoOpen = "XXYXX";
	//
	// }
	//
	// // this case is fetching posts from Internal Search jazz
	// else if (((MainActivity) getActivity()).FeedURLtoOpen !=
	// "INTERNAL_SEARCH") {
	// allPosts = db.searchPosts(((MainActivity)
	// getActivity()).INTERNAL_SEARCH_STRING); // <---- this will return a List
	// of Posts that contain the phrase "iPad" in the title;
	// }
	//
	// else {
	// allPosts = db.getAllPosts();
	// }
	// ((MainActivity) getActivity()).FeedURLtoOpen = "XXYXX";
	// ((MainActivity) getActivity()).INTERNAL_SEARCH_STRING = "";
	// db.closeDB();
	// System.out.println("size of all posts for this feed: " +
	// allPosts.size());
	// for (int i = 0; i < allPosts.size(); i++) {
	// // if(!allPosts.get(i).getRead()){
	// titles.add(allPosts.get(i).getTitle());
	// URLs.add(allPosts.get(i).getUrl());
	// keys.add(allPosts.get(i).getKey());
	// readStatus.add(allPosts.get(i).getRead());
	// ((MainActivity)
	// getActivity()).SHARED_POSTS_URLs.add(allPosts.get(i).getUrl());
	// // }
	// // else
	// // System.out.println("found read post:" + i);
	// }
	//
	// }

	@Override
	public void onDestroyView() {
		// ((MainActivity) getActivity()).FeedURLtoOpen = "";
		((MainActivity) getActivity()).INTERNAL_SEARCH_STRING = "";

		super.onDestroyView();
	}

	public void reDrawListView() { // call when need to redraw
		populateLists(false);
		
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.refresh, menu);
	}

	private void loadPage() {

		if (((MainActivity) getActivity()).FeedURLtoOpen.isEmpty())
			MainActivity.browser.simplecta_all();
		else if (((MainActivity) getActivity()).FeedURLtoOpen != "INTERNAL_SEARCH")
			MainActivity.browser
					.simplecta_feed(((MainActivity) getActivity()).FeedURLtoOpen);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		case R.id.action_refresh:
			reDrawListView();
			loadPage();
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}