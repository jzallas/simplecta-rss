package com.capstone.rss;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;


public class webViewFrag extends Fragment{
   
	private SimplectaWebView webView;
//	private Button navButton;
	
	private static final int SWIPE_MIN_DISTANCE = 20; // 8;
	private static final int SWIPE_MAX_OFF_PATH = 150; // 280 // 200;
	private static final int SWIPE_THRESHOLD_VELOCITY = 200;
	private GestureDetector gestureDetector; // imported GestureDetector
	View.OnTouchListener gestureListener;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle saved)
        {
        View v = inflater.inflate(R.layout.webview_frag, group, false);
//        ((MainActivity) getActivity()).setTitle(((MainActivity) getActivity()).TitleInMainBar);
//        navButton = (Button)v.findViewById(R.id.Navigationbutton);
        setupWebView(v);
        setHasOptionsMenu(true);
        return v;
        }
    
    
    protected void setupWebView(View v){
    	class customWV extends WebViewClient{
    		@Override
    		public boolean shouldOverrideUrlLoading(WebView view, String url) {
    			view.loadUrl(url);
    			return true;
    		}

    		@Override
    		public void onPageFinished(WebView view, String url){
    			//if a page is ever loaded in this fragment and its simplecta, it was during a login so swap out
    			if (url.contains("simplecta") && ((MainActivity)getActivity()).loggedIn()){
    				webView.setWebViewClient(new WebViewClient()); //disconnect the custom client

    				FragmentTransaction ft = ((MainActivity)getActivity()).fm.beginTransaction();
    				ft.replace(R.id.MAINFRAG, ((MainActivity)getActivity()).postfragment);
    				ft.commit();
    			}
    			view.clearHistory();
    			view.clearCache(true);
    			
    			// is im not on main page
//    			if(((MainActivity)getActivity()).loggedIn()){
//	    			final Handler handler = new Handler();
//	    			handler.postDelayed(new Runnable() {
//	    			  @Override
//	    			  public void run() {
//	    			    //Do something after 100ms
//	    				  navButton.setVisibility(View.GONE);
//	    			  }
//	    			}, 2000);		
//    			}	
    		}
    	}
        
    	webView = (SimplectaWebView) v.findViewById(R.id.webviewfrag);
    	webView.setWebViewClient(new customWV());
  	    webView.getSettings().setJavaScriptEnabled(true);
  	    
  	    gestureDetector = new GestureDetector(new MyGestureDetector());
		gestureListener = new View.OnTouchListener() {
			public boolean onTouch(View v, MotionEvent event) {
				return gestureDetector.onTouchEvent(event);
			}
		};
		
		webView.setOnTouchListener(gestureListener);
		
  	    if(!webView.isNetworkConnected()) Toast.makeText((MainActivity)getActivity(),"No network connection", Toast.LENGTH_SHORT).show();
  	    else webView.loadUrl(MainActivity.URLtoOpen);
    }
	    
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		   inflater.inflate(R.menu.openinbrowser, menu);
		   inflater.inflate(R.menu.share_post, menu);
		}
	@SuppressWarnings("static-access")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		
		case R.id.action_openinbrowser:
			Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(((MainActivity)getActivity()).URLtoOpen));
			startActivity(browserIntent);
			return true;
			
		case R.id.action_share_post:
			sharePost((((MainActivity)getActivity()).URLtoOpen));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	
	private void sharePost(String uri)
	{
		System.out.println("Sharing linK: " + uri);
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		shareIntent.setType("text/plain");
		shareIntent.putExtra(Intent.EXTRA_TEXT, uri);
		startActivity(Intent.createChooser(shareIntent, "Share..."));
	}
	
	
	class MyGestureDetector extends SimpleOnGestureListener { // imported
		// SimpleOnGestureListener
		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
				float velocityY) {
			try {
				if(e1.getY() > 200){
					return false;
				}
				if (Math.abs(velocityY) > Math.abs(velocityX)) {
					if (e1.getY() - e2.getY() > SWIPE_MIN_DISTANCE // ){
							&& Math.abs(velocityY) > SWIPE_THRESHOLD_VELOCITY) {
						onSwipeTop();
					} else {
						onSwipeBottom();
					}
				}
				else if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE // ){
						&& Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
					onSwipeLeft();

				} else { 
					onSwipeRight();
				}
			} catch (Exception e) {
				// nothinG
			}
			return false;
		}

		private void onSwipeLeft() {
			if(((MainActivity) getActivity()).currentPost == 0)
				return;
			int nextPost = --((MainActivity) getActivity()).currentPost;
			((MainActivity)getActivity()).URLtoOpen = ((MainActivity) getActivity()).SHARED_POSTS_URLs.get(nextPost);
			webView.loadUrl(((MainActivity) getActivity()).SHARED_POSTS_URLs.get(nextPost));
		}

		private void onSwipeRight() {
			if(((MainActivity) getActivity()).currentPost >= ((MainActivity) getActivity()).SHARED_POSTS_URLs.size() )
				return;
			int nextPost = ++((MainActivity) getActivity()).currentPost;
			((MainActivity)getActivity()).URLtoOpen = ((MainActivity) getActivity()).SHARED_POSTS_URLs.get(nextPost);
			webView.loadUrl(((MainActivity) getActivity()).SHARED_POSTS_URLs.get(nextPost));
		}

		private void onSwipeBottom() {
			// TODO Auto-generated method stub
			System.out.println("bottom");
			
		}

		private void onSwipeTop() {
			// TODO Auto-generated method stub
			System.out.println("top");

		}
	}
}