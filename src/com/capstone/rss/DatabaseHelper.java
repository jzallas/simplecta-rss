package com.capstone.rss;
 
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
 
public class DatabaseHelper extends SQLiteOpenHelper {
 
    // Database Version
    private static final int DATABASE_VERSION = 1;
 
    // Database Name
    private static final String DATABASE_NAME = "simplecta";
 
    // Table Names
    private static final String TABLE_FEEDS = "feeds";
    private static final String TABLE_POSTS = "posts";
    private static final String TABLE_FEED_POSTS = "feed_posts";
 
    // Common column names
    private static final String KEY_ID = "id";
    private static final String KEY_URL = "url";
    private static final String KEY_KEY = "key";
    private static final String KEY_TITLE = "title";
    
    // post column names
    private static final String KEY_READ = "read";
 
    // FEED_POSTS Table - column names
    private static final String KEY_POST_ID = "post_id";
    private static final String KEY_FEED_ID = "feed_id";
 
    // Table Create Statements
    // FEEDS table create statement
    private static final String CREATE_TABLE_FEEDS = "CREATE TABLE "
    		+ TABLE_FEEDS + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_URL
            + " TEXT UNIQUE," + KEY_KEY + " TEXT," + KEY_TITLE
            + " TEXT" + ")";
 
    // POSTS table create statement
    private static final String CREATE_TABLE_POSTS = "CREATE TABLE " + TABLE_POSTS + "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_URL
            + " TEXT UNIQUE," + KEY_KEY + " TEXT," + KEY_TITLE
            + " TEXT," + KEY_READ + " INTEGER" + ")";
 
    // FEED_POSTS table create statement
    private static final String CREATE_TABLE_FEED_POSTS = "CREATE TABLE "
            + TABLE_FEED_POSTS + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_FEED_ID + " INTEGER," + KEY_POST_ID + " INTEGER" + ")";
 
    public DatabaseHelper(Context context, String username) {
        super(context, DATABASE_NAME + username, null, DATABASE_VERSION);
    }
 
    @Override
    public void onCreate(SQLiteDatabase db) {
 
        // creating required tables
        db.execSQL(CREATE_TABLE_FEEDS);
        db.execSQL(CREATE_TABLE_POSTS);
        db.execSQL(CREATE_TABLE_FEED_POSTS);
    }
 
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FEEDS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_POSTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FEED_POSTS);
 
        // create new tables
        onCreate(db);
    }
 
    // ------------------------ "posts" table methods ----------------//
 
     /**
     * Creating a post
     */
    public void insertPost(Post post, Feed feed) {
        SQLiteDatabase db = this.getWritableDatabase();
        
        int feed_id = getFeedID(feed.getUrl());
 
        ContentValues values = new ContentValues();
        values.put(KEY_URL, post.getUrl());
        values.put(KEY_KEY, post.getKey());
        values.put(KEY_TITLE, post.getTitle());
        values.put(KEY_READ, post.getRead());
 
        // insert row
        
        if (db.update(TABLE_POSTS, values, KEY_URL + " =?", new String[] {post.getUrl()})==0)
        {
        	long post_id = db.insert(TABLE_POSTS, null, values);
        	// insert feed_ids
                createFeedPost(post_id, feed_id);
        }
    }
    
    public List<Post>searchPosts(String word)
    {
        List<Post> posts = new ArrayList<Post>();
    	SQLiteDatabase db = this.getReadableDatabase();
 
        String selectQuery = "SELECT  * FROM " + TABLE_POSTS + " WHERE "
                + KEY_TITLE + " LIKE '%" + word +"%'";
        
        Cursor c = db.rawQuery(selectQuery, null);
        
        if (c.moveToFirst()) {
            do {
                Post post = new Post();
                post.setID(c.getInt(c.getColumnIndex(KEY_ID)));
                post.setUrl(c.getString(c.getColumnIndex(KEY_URL)));
                post.setKey(c.getString(c.getColumnIndex(KEY_KEY)));
                post.setTitle(c.getString(c.getColumnIndex(KEY_TITLE)));
                if (c.getInt(c.getColumnIndex(KEY_READ)) == 0)
                	post.setRead(false);
                else
                	post.setRead(true);

                // adding to feed list
                posts.add(post);
            } while (c.moveToNext());
        }
 
        return posts;
 
    }
 
    /**
     * get single post
     */
    public Post getPost(long post_id) {
        SQLiteDatabase db = this.getReadableDatabase();
 
        String selectQuery = "SELECT  * FROM " + TABLE_POSTS + " WHERE "
                + KEY_ID + " = " + post_id;
 
        Log.e("SINGLE POST QUERY", selectQuery);
 
        Cursor c = db.rawQuery(selectQuery, null);
 
        if (c != null)
            c.moveToFirst();
 
        Post post = new Post();
        post.setID(c.getInt(c.getColumnIndex(KEY_ID)));
        post.setUrl(c.getString(c.getColumnIndex(KEY_URL)));
        post.setKey(c.getString(c.getColumnIndex(KEY_KEY)));
        post.setTitle(c.getString(c.getColumnIndex(KEY_TITLE)));
        if (c.getInt(c.getColumnIndex(KEY_READ)) == 0)
        	post.setRead(false);
        else
        	post.setRead(true);

        return post;
    }
    
    public int getPostID(String Url){
    	int ID = -1;
    	String selectQuery = "SELECT " + KEY_ID + " FROM " + TABLE_POSTS + " WHERE " +
    			KEY_URL + " = '" + Url + "';";
    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(selectQuery, null);
    	if (c.moveToFirst()) {
            do {
            	ID = c.getInt(c.getColumnIndex(KEY_ID));
            }while(c.moveToNext());
    	}
    	return ID;
    }
 
    /**
     * getting all posts
     * */
    public List<Post> getAllPosts() {
        List<Post> posts = new ArrayList<Post>();
        String selectQuery = "SELECT  * FROM " + TABLE_POSTS;
 
        Log.e("ALL POST QUERY", selectQuery);
 
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Post post = new Post();
                post.setID(c.getInt(c.getColumnIndex(KEY_ID)));
                post.setUrl(c.getString(c.getColumnIndex(KEY_URL)));
                post.setKey(c.getString(c.getColumnIndex(KEY_KEY)));
                post.setTitle(c.getString(c.getColumnIndex(KEY_TITLE)));
                if (c.getInt(c.getColumnIndex(KEY_READ)) == 0)
                	post.setRead(false);
                else
                	post.setRead(true);

                // adding to feed list
                posts.add(post);
            } while (c.moveToNext());
        }
 
        return posts;
    }
 
    /**
     * getting all posts under single feed
     * */
    public List<Post> getAllPostsByFeed(String feed_url) {
        List<Post> posts = new ArrayList<Post>();
 
        
        int feed_id = getFeedID(feed_url);
        System.out.println("ID in db: " + feed_id);
        String selectQuery = "SELECT * FROM " + TABLE_POSTS + " p, " + TABLE_FEED_POSTS + " fp WHERE p." + KEY_ID + " = "
                + "fp." + KEY_POST_ID + " AND fp." + KEY_FEED_ID + " = " + feed_id + ";";
        
 
        Log.e("ALL POST BY FEED QUERY", selectQuery);
 
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Post post = new Post();
                post.setID(c.getInt(c.getColumnIndex(KEY_ID)));
                post.setUrl(c.getString(c.getColumnIndex(KEY_URL)));
                post.setKey(c.getString(c.getColumnIndex(KEY_KEY)));
                post.setTitle(c.getString(c.getColumnIndex(KEY_TITLE)));
                if (c.getInt(c.getColumnIndex(KEY_READ)) == 0)
                	post.setRead(false);
                else
                	post.setRead(true);
                // adding to post list
                posts.add(post);
            } while (c.moveToNext());
        }
        return posts;
    }
    /**
     * getting post count
     */
    public int getPostCount() {
        String countQuery = "SELECT  * FROM " + TABLE_POSTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
 
        int count = cursor.getCount();
        cursor.close();
 
        // return count
        return count;
    }
 
    /**
     * Updating a Post
     */
    public int updatePost(Post post) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_URL, post.getUrl());
        values.put(KEY_KEY, post.getKey());
        values.put(KEY_TITLE, post.getTitle());
        values.put(KEY_READ, post.getRead());
 
        // updating row
        return db.update(TABLE_POSTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(post.getID())});
    }
 
    /**
     * Deleting a Post
     */
    public void deletePost(long post_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_POSTS, KEY_ID + " = ?",
                new String[] { String.valueOf(post_id) });
        db.delete(TABLE_FEED_POSTS, KEY_POST_ID + " = ?",
                new String[] { String.valueOf(post_id) });
    }
 
    // ------------------------ "feeds" table methods ----------------//
 
    /**
     * Creating feed
     */
    public void insertFeed(Feed feed) {
        SQLiteDatabase db = this.getWritableDatabase();
 
//        String insertQuery = "INSERT OR IGNORE INTO " + TABLE_FEEDS +
//        		" (" + KEY_URL + ", "+ KEY_KEY + ", "+ KEY_TITLE +
//        				") VALUES ('" + feed.getUrl() + "', '" + feed.getKey() + "', '"+ feed.getTitle() + "');";
//
//        System.out.println(insertQuery);
//
//        String checkQuery = "UPDATE "+ TABLE_FEEDS + " SET " + KEY_URL + " = '" + feed.getUrl() + "', " +
//        		KEY_KEY +" = '" + feed.getKey() + "', " + KEY_TITLE + " = '" + feed.getTitle() + "' WHERE " +
//        		KEY_URL + "='" + feed.getUrl() + "';";
//        
//        System.out.println(checkQuery);
        
        ContentValues values = new ContentValues();
        values.put(KEY_URL, feed.getUrl());
        values.put(KEY_KEY, feed.getKey());
        values.put(KEY_TITLE, feed.getTitle());
        
        // insert row
        
        if (db.update(TABLE_FEEDS, values, KEY_URL + "=?", new String[] {feed.getUrl()})==0)
        	db.insert(TABLE_FEEDS, null, values);
    }
    
    /**
     * getting feed count
     */
    public int getFeedCount() {
        String countQuery = "SELECT  * FROM " + TABLE_FEEDS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
 
        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }
 
    
    public int getFeedID(String Url){
    	int ID = -1;
    	String selectQuery = "SELECT " + KEY_ID + " FROM " + TABLE_FEEDS + " WHERE " +
    			KEY_URL + " = '" + Url + "';";
    	SQLiteDatabase db = this.getReadableDatabase();
    	Cursor c = db.rawQuery(selectQuery, null);
    	if (c.moveToFirst()) {
            do {
            	ID = c.getInt(c.getColumnIndex(KEY_ID));
            }while(c.moveToNext());
    	}
    	return ID;
    }
 
    /**

     * getting feed for a specific post

     * */

    public Feed getFeedOfPost(String url) {

        Feed feed = null;

 

        

        int post_id = getPostID(url);

        String selectQuery = "SELECT * FROM " + TABLE_FEEDS + " f, " + TABLE_FEED_POSTS + " fp WHERE f." + KEY_ID + " = "

                + "fp." + KEY_FEED_ID + " AND fp." + KEY_POST_ID + " = " + post_id + ";";

        

 

 

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery(selectQuery, null);

 

        // looping through all rows and adding to list

        if (c.moveToFirst()) {

            do { 

            	feed = new Feed();

                feed.setID(c.getInt(c.getColumnIndex(KEY_ID)));

                feed.setUrl(c.getString(c.getColumnIndex(KEY_URL)));

                feed.setKey(c.getString(c.getColumnIndex(KEY_KEY)));

                feed.setTitle(c.getString(c.getColumnIndex(KEY_TITLE)));

            } while (c.moveToNext());

        }

        return feed;

    }

    
    /**
     * getting all feeds
     * */
    public List<Feed> getAllFeeds() {
        List<Feed> feeds = new ArrayList<Feed>();
        String selectQuery = "SELECT * FROM " + TABLE_FEEDS;
 
        Log.e("ALL FEEDS QUERY", selectQuery);
 
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Feed feed = new Feed();
                feed.setID(c.getInt(c.getColumnIndex(KEY_ID)));
                feed.setUrl(c.getString(c.getColumnIndex(KEY_URL)));
                feed.setKey(c.getString(c.getColumnIndex(KEY_KEY)));
                feed.setTitle(c.getString(c.getColumnIndex(KEY_TITLE)));
                // adding to Feeds list
                feeds.add(feed);
            } while (c.moveToNext());
        }
        return feeds;
    }
 
    /**
     * Updating a feed
     */
    public int updateFeed(Feed feed) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_URL, feed.getUrl());
        values.put(KEY_KEY, feed.getKey());
        values.put(KEY_TITLE, feed.getTitle());
 
        // updating row
        return db.update(TABLE_FEEDS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(feed.getID()) });
    }
 
    /**
     * Deleting a feed
     */
    public void deleteFeed(String url, boolean should_delete_all_post_feeds) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        // before deleting feed
        // check if posts under this feed should also be deleted
        if (should_delete_all_post_feeds) {
            // get all posts under this feed
            List<Post> allFeedsToPosts = getAllPostsByFeed(url);
 
            // delete all posts
            for (Post post : allFeedsToPosts) {
                // delete post
                deletePost(post.getID());
            }
        }
        System.out.println("deleting feed ---> " + TABLE_FEEDS + " " + KEY_ID + " = ?" + String.valueOf(getFeedID(url)));
        db.delete(TABLE_FEEDS, KEY_ID + " = ?",
                new String[] { String.valueOf(getFeedID(url)) });
    }
 
    // ------------------------ "feed_posts" table methods ----------------//
 
    /**
     * Creating feed_posts
     */
    private long createFeedPost(long post_id, long feed_id) {
        SQLiteDatabase db = this.getWritableDatabase();
 
        ContentValues values = new ContentValues();
        values.put(KEY_POST_ID, post_id);
        values.put(KEY_FEED_ID, feed_id);
 
        long id = db.insert(TABLE_FEED_POSTS, null, values);
 
        return id;
    }
    
    private List<Feed> getAllFeedPosts() {
        List<Feed> feeds = new ArrayList<Feed>();
        String selectQuery = "SELECT * FROM " + TABLE_FEED_POSTS;
 
        Log.e("ALL FEEDS_POST QUERY", selectQuery);
 
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
 
        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                Feed feed = new Feed();
                feed.setID(c.getInt(c.getColumnIndex(KEY_ID)));
                feed.setUrl(c.getInt(c.getColumnIndex(KEY_POST_ID))+"");
                feed.setTitle(c.getInt(c.getColumnIndex(KEY_FEED_ID))+"");
                // adding to Feeds list
                feeds.add(feed);
            } while (c.moveToNext());
        }
        return feeds;
    }

    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
}