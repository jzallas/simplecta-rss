package com.capstone.rss;

public class Post {
	private int id;
	private String link = null;
	private String title = null;
	private String key = null;
	private boolean read = false;
	
	public Post()
	{
	}
	
	public Post(String title, String link){
		this.link = link;
		this.title = title;
		
	}
	protected void setUrl(String url) {
//		url = url.replace("/feed/", "");
//		url = url.replace("%3a", ":");
//		url = url.replace("%2f", "/");
//		if (url.startsWith("?")){
//			url = url.substring(1);
//		}
		this.link = url;
		
	}

	protected String getUrl() {
		return link;
	}
	
	protected void setID(int id){
		this.id = id;
	}
	
	protected int getID(){
		return id;
	}
	
	protected String getTitle()
	{
		return title;
	}
	
	protected void setTitle(String title){
		this.title = title;
	}
	
	protected void setKey(String key){
//		key = key.replace("/unsubscribe/", "");
//		if (key.startsWith("?")){
//			key = key.substring(1);
//		}
		this.key = key;
	}
	
	protected String getKey(){
		return key;
	}
	
	protected void setRead(boolean read){
		this.read = read;
	}
	
	protected boolean getRead(){
//		boolean isRead;
//		isRead = read;
		return read;
	}
}
