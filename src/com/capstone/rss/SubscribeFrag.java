package com.capstone.rss;



import android.os.Bundle;
import android.app.Fragment;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

public class SubscribeFrag extends Fragment  implements OnClickListener{
	
	private EditText Subscribe_TextField;
	Button Subscribe_To_Feed_Button;
	
	@Override
   public View onCreateView(LayoutInflater inflater,
      ViewGroup container, Bundle savedInstanceState) {
		
		RelativeLayout myView = (RelativeLayout) inflater.inflate(R.layout.activity_subscribe, container, false);
		
		Subscribe_To_Feed_Button = (Button)myView.findViewById(R.id.button_subscribe_to_feed);
		Subscribe_To_Feed_Button.setOnClickListener( this);
		Subscribe_TextField = (EditText)myView.findViewById(R.id.Subscribe_TextField);
		
//		return inflater.inflate(R.layout.activity_feeds, container, false);
		return myView; // super.onCreateView(inflater, container, savedInstanceState);
   } // 
	
	private void subscribeToFeedByFeedURL(final String feedURL )
	{
//		getActivity().runOnUiThread(new Runnable() {
//	        @Override
//	        public void run() {
				if( MainActivity.browser.simplecta_addRSS(feedURL) ){

				} else {
//					Toast toast = Toast.makeText(getActivity(), "Can't subscribe. Please check your network connection",
//							Toast.LENGTH_SHORT);
//					toast.show();
				}
//	        }
//		});
	}

	public void toastMessageForSubscribe(boolean flag)
	{
		System.out.println("calling toast for subscribe" + flag);
		if(flag == true){
			Toast toast = Toast.makeText(getActivity(), "Subscribed to feed! ",
			Toast.LENGTH_SHORT);
			toast.show();
		}
		else{
			Toast toast = Toast.makeText(getActivity(), "Can't subscribe. ",
					Toast.LENGTH_SHORT);
			toast.show();
		}
		
	}
		
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {		
			case R.id.button_subscribe_to_feed:
				String q = Subscribe_TextField.getText().toString();
				System.out.println("string is : " + q );
				subscribeToFeedByFeedURL(q);
				Subscribe_TextField.setText("");
				final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
		        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
				break;
		}
		
	}
}
