package com.capstone.rss;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemLongClickListener;

public class FeedsFrag extends ListFragment{
	protected ArrayList<String> Feeds = new ArrayList<String>();
	private ArrayList<String> URLs = new ArrayList<String>();
	private ArrayList<String> keys = new ArrayList<String>();
	private ArrayList<String> titles = new ArrayList<String>();
//	private ArrayList<String> readStatus = new ArrayList<String>();
	
	private ArrayList<Post> allPosts = new ArrayList<Post>();
	
	private ArrayAdapter<String> adapter;

	@SuppressWarnings("deprecation")
	@Override
	public void onListItemClick(ListView l, View v, final int position, long id) {		
		adapter.clear();
		if(allPosts.size() == 0){
			System.out.println("on feeds page. number of titles: " + titles.size());
			populatePostsFromFeed(URLs.get(position), titles.get(position));
		} 
	}
	
	 @Override
	    public void onActivityCreated(Bundle savedInstanceState) {
	        super.onActivityCreated(savedInstanceState);
	        OnItemLongClickListener listener = new OnItemLongClickListener() {
	        	
	        	@SuppressWarnings("deprecation")
//	        	@Override
	            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, final int position, long id) {
	            	AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
	        		alertDialog.setTitle("Options");
	        		alertDialog.setMessage("Feed: " + titles.get(position));
	        		alertDialog.setButton("Cancel", new DialogInterface.OnClickListener() {
	        		   public void onClick(DialogInterface dialog, int which) {
	        			   System.out.println("Clicked Cancel ");
	        		   }
	        		});
	        		alertDialog.setButton2("Unsubscribe", new DialogInterface.OnClickListener() {
	        			public void onClick(DialogInterface dialog, int which) {
	        			   unsubscribeFromFeedByFeedURLAndKey(URLs.get(position), keys.get(position));
	        			   System.out.println("Clicked Unsubscribe ");
	        			   adapter.notifyDataSetChanged();
	        		   }
	        		});
	        		alertDialog.setButton3("Share", new DialogInterface.OnClickListener() {
	        			   public void onClick(DialogInterface dialog, int which) {
	        			      // TODO Add your code for the button here.
	        				   shareFeed(URLs.get(position), titles.get(position)); 
	        				   System.out.println("Clicked Share ");
	        			   }
	        			});

	        		alertDialog.show();
	                
	                return true;
	            }

	        };	 
	        getListView().setOnItemLongClickListener(listener);
	    }
	
	@Override
    public View onCreateView(LayoutInflater inflater,
      ViewGroup container, Bundle savedInstanceState) {
		((MainActivity) getActivity()).SHARED_POSTS_URLs.clear();
//		((MainActivity) getActivity()).setTitle("All feeds");
		ActionBar ab =((MainActivity) getActivity()).getActionBar();
		ab.setTitle("Feeds");
		adapter = new ArrayAdapter<String>(
				inflater.getContext(), android.R.layout.simple_list_item_1, Feeds);
		populateFeeds();
		setListAdapter(adapter);
		return super.onCreateView(inflater, container, savedInstanceState);
   }
	
	private void openPost(final String url)
	{
		System.out.println("opening post: " + url);
		getActivity().runOnUiThread(new Runnable() {
	        @Override
	        public void run() {
	        	MainActivity.browser.loadUrl(url);
	        }});
	}
	
	private void populateFeeds()
	{
		DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext(), MainActivity.username);
		Feeds.clear();
		URLs.clear();
		keys.clear();
		List<Feed> allFeeds = db.getAllFeeds();
		db.closeDB();
		for (int i = 0; i < allFeeds.size(); i++) {
			Feeds.add(allFeeds.get(i).getTitle());
			URLs.add(allFeeds.get(i).getUrl());
			keys.add(allFeeds.get(i).getKey());
			titles.add(allFeeds.get(i).getTitle());
		}		
		//
	}
	
	void getPostsFromFeedViaSimplecta(final String feed_url)
	{
		getActivity().runOnUiThread(new Runnable() {
	        @Override
	        public void run() {
				if( MainActivity.browser.simplecta_feed(feed_url)) {	
				} 
				else {
					Toast toast = Toast.makeText(getActivity(), "Can't get the posts. Please check your network connection",
							Toast.LENGTH_SHORT);
					toast.show();	
				}
	        }});
	  }
	private void shareFeed(String feedURL, String title)
	{
		System.out.println("Sharing Feed: " + title);
		System.out.println("Link is: " + feedURL);
		Intent shareIntent = new Intent(Intent.ACTION_SEND);
		  shareIntent.setType("text/plain");
		  shareIntent.putExtra(Intent.EXTRA_TEXT, feedURL + "\n" + title);
		  startActivity(Intent.createChooser(shareIntent, "Share..."));
	}
	
	private void unsubscribeFromFeedByFeedURLAndKey(String feedURL, String feedKey )
	{
		DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext(), MainActivity.username);
		System.out.println("trying to unsubscribe from: " + feedURL);
		db.deleteFeed(feedURL, true); //
		db.closeDB();
		unsubscribeFromFeedInSimplecta(feedKey, feedURL);
//		adapter.clear();
        
        
        titles.clear();
		URLs.clear();
		keys.clear();
		//MainActivity.browser.simplecta_all();
		DatabaseHelper db1 = new DatabaseHelper(getActivity().getApplicationContext(), MainActivity.username);
		List<Feed> allFeeds;
		allFeeds = db1.getAllFeeds();
		db1.closeDB();
		
		for (int i = allFeeds.size()-1; i > -1; i--)		{
			titles.add(allFeeds.get(i).getTitle());
			URLs.add(allFeeds.get(i).getUrl());
			keys.add(allFeeds.get(i).getKey());
		}			
		
		
	}
	
	private boolean resumeHasRun = false;

	@Override
	public void onResume() {
	    super.onResume();
	    if (!resumeHasRun) {
	        resumeHasRun = true;
	        return;
	    }
	    adapter.notifyDataSetChanged();
	}
 
	private void unsubscribeFromFeedInSimplecta(final String link, final String url)
	{
		getActivity().runOnUiThread(new Runnable() {
	        @Override
	        public void run() {
				if( MainActivity.browser.simplecta_unsubscribe(link)) {
					// unsubscribe from Simplecta Server's
					Toast toast = Toast.makeText(getActivity(), "Unsubscribed from " + url,
							Toast.LENGTH_SHORT);
					toast.show();	
				} 
				else {
					Toast toast = Toast.makeText(getActivity(), "Can't unsubscribe. Please check your network connection",
							Toast.LENGTH_SHORT);
					toast.show();	
				}
	        }});
	}
	private void populatePostsFromFeed( String pos, String titleOfFeed) // URL to feed
	{
		((MainActivity) getActivity()).FeedURLtoOpen = pos;
		((MainActivity) getActivity()).TitleInMainBar = titleOfFeed;
		System.out.println("should be opening posts fragment of feed: " + ((MainActivity) getActivity()).FeedURLtoOpen);
		FragmentTransaction ft = ((MainActivity) getActivity()).fm
				.beginTransaction();
		ft.replace(R.id.MAINFRAG,
				((MainActivity) getActivity()).postfragment);
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
		
		ft.addToBackStack(null);
		ft.commit();
	}
	
	
}