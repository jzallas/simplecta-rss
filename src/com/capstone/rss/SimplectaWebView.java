package com.capstone.rss;


import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Toast;

public class SimplectaWebView extends WebView {
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreateContextMenu(ContextMenu menu) {
	    super.onCreateContextMenu(menu);

	    HitTestResult result = getHitTestResult();

	    MenuItem.OnMenuItemClickListener handler = new MenuItem.OnMenuItemClickListener() {
	        public boolean onMenuItemClick(MenuItem item) {
	                System.out.println("onMenuItemClick CALED");
	                return true;
	        }
	    };
	    String copiedString = result.getExtra();
	    if(copiedString == null)
	    	return;
	    if(copiedString.length() == 0)
	    	return;
	    if(copiedString.contains("rss") || copiedString.contains("xml")){
//	    		copiedString.matches(".*\\.xml") || copiedString.matches(".*\\.rss")) {
	    	int sdk = android.os.Build.VERSION.SDK_INT;
	        if(sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
	            android.text.ClipboardManager clipboard = (android.text.ClipboardManager)c.getSystemService(c.CLIPBOARD_SERVICE);
	            clipboard.setText(copiedString);
	        } else {
	            android.content.ClipboardManager clipboard = (android.content.ClipboardManager)c.getSystemService(c.CLIPBOARD_SERVICE); 
	            android.content.ClipData clip = android.content.ClipData.newPlainText("text label",copiedString);
	            clipboard.setPrimaryClip(clip);
	        }
	        Toast toast = Toast.makeText( c, "RSS Feed Link copied to clipboad", Toast.LENGTH_SHORT);
			toast.show();	
	    }else {
		    Toast toast = Toast.makeText( c, "Please long click feed URL!", Toast.LENGTH_SHORT);
			toast.show();
	    }
	    
	    
	    
	    
	    
//	    if (result.getType() == HitTestResult.IMAGE_TYPE ||
//	            result.getType() == HitTestResult.SRC_IMAGE_ANCHOR_TYPE) {
//	        // Menu options for an image.
//	        //set the header title to the image url
//	        menu.setHeaderTitle(result.getExtra());
//	        menu.add(0, ID_SAVEIMAGE, 0, "Save Image").setOnMenuItemClickListener(handler);
//	        menu.add(0, ID_VIEWIMAGE, 0, "View Image").setOnMenuItemClickListener(handler);
//	    } else if (result.getType() == HitTestResult.ANCHOR_TYPE ||
//	            result.getType() == HitTestResult.SRC_ANCHOR_TYPE) {
//	        // Menu options for a hyperlink.
//	        //set the header title to the link url
//	        menu.setHeaderTitle(result.getExtra());
//	        menu.add(0, ID_SAVELINK, 0, "Save Link").setOnMenuItemClickListener(handler);
//	        menu.add(0, ID_SHARELINK, 0, "Share Link").setOnMenuItemClickListener(handler);
//	    }
	}
	
	private Context c;

	protected static final String simplectaURL = "http://simplecta.appspot.com/";

	public SimplectaWebView(Context context) {
		super(context);
		c = context;
	}

	public SimplectaWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		c = context;
	}

	public SimplectaWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		c = context;
	}
	public SimplectaWebView(Context context, AttributeSet attrs, int defStyle,
			boolean privateBrowsing) {
		super(context, attrs, defStyle, privateBrowsing);
		c = context;
	}
	
	
	protected boolean isNetworkConnected() {
		  ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo ni = cm.getActiveNetworkInfo();
		  if (ni == null) {
		   // There are no active networks.
		   return false;
		  } else
		   return true;
		 }
	
	// *******FUNCTIONS TO ACCESS SIMPLECTA********//

		// add a feed to list on simplecta using the rss url
		boolean simplecta_addRSS(String rssUrl) {
			try {
				if(isNetworkConnected()){
					loadUrl(simplectaURL + "addRSS/?url=" + rssUrl);
					return true;
				}
				else return false;
			} catch (Exception e) {
				return false;
			}
		}

		// unsubscribe from feed on simplecta using key from html
		boolean simplecta_unsubscribe(String feedKey) {
			try {
				if(isNetworkConnected()&& feedKey!=null){
				loadUrl(simplectaURL + "unsubscribe/?" + feedKey);
				return true;
				}
				else return false;
			} catch (Exception e) {
				return false;
			}
		}

		// load a specific feed on simplecta using the rss url
		boolean simplecta_feed(String rssUrl) {
			try {
				if(isNetworkConnected()){
					loadUrl(simplectaURL + "feed/?" + rssUrl);
					return true;
				}
				else return false;
			} catch (Exception e) {
				return false;
			}
		}

		// load the main page of simplecta which shows read/unread
		boolean simplecta_all() {
			try {
				if(isNetworkConnected()){
					loadUrl(simplectaURL);
					return true;
				}
				else return false;
			} catch (Exception e) {
				return false;
			}
		}

		// loads manage subscription page of simplecta which shows all feeds
		boolean simplecta_manage() {
			try {
				if (isNetworkConnected()) {
					//
					loadUrl(simplectaURL + "feeds/");
	
					return true;
				} else
					return false;
			} catch (Exception e) {
				return false;
			}
		}

		// send markRead request using key from html
		boolean simplecta_markRead(String postKey) {
			try {
				if(isNetworkConnected()&& postKey!=null){
					loadUrl(simplectaURL + "markRead/?" + postKey);
					return true;
				}
				else return false;
			} catch (Exception e) {
				return false;
			}
		}

		// send markUnread request using key from html
		boolean simplecta_markUnread(String postKey) {
			try {
				if(isNetworkConnected() && postKey!=null){
					loadUrl(simplectaURL + "markUnread/?" + postKey);
					return true;
				}
				else return false;
			} catch (Exception e) {
				return false;
			}
		}

}
